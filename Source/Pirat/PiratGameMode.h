// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Blueprint/UserWidget.h"
#include "GameFramework/GameMode.h"
#include "PiratGameMode.generated.h"

/**
 * 
 */
UCLASS()
class PIRAT_API APiratGameMode : public AGameMode
{
	GENERATED_BODY()
	
	


public:
	// Sets default values for this
	APiratGameMode();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	/** Remove the current menu widget and create a new one from the specified class */
//	UFUNCTION(BlueprintCallable, Category = "UMG Interface")
//		void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidget);



	


	//  WIND 
	/** Template for Wind Speed and Direction */
		FRotator WindDirectionChange;
	UFUNCTION(BlueprintPure, Category = "World")
		FRotator GetWindDirection();
	UFUNCTION(BlueprintPure, Category = "World")
		uint8 GetWindSpeed();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "World")
		uint8 WindSpeed;

protected:
	//  User Interface - UMG HUD Widget
	/** The HUD widget class for game start */
//	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG Interface")
//		TSubclassOf<UUserWidget> StartingWidget;
//	/** The current used HUD widget */
//	    UUserWidget* CurrentWidget;



	//  WIND 
	/** TimerHandle for WindChangeTimer */
	FTimerHandle WindChange_TimerHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "World")
		FRotator WindDirection;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "World")
		int WindSpeedChangeDelayMin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "World")
		int WindSpeedChangeDelayMax;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "World")
		int WindDirectionChangeDelayMin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "World")
		int WindDirectionChangeDelayMax;

	// Template for Wind Speed and Direction
	void WindChange();



	/** The rate/speed at witch Ship hull loses durability */
	float HullDecayRate;


private:
	// Wind Speed
	//int WindSpeed;
	//  How often Wind Direction change
	int WindDirectionChangeDelay;
	// How often Wind Speed change
	int WindSpeedChangeDelay;



	//-------------------   HULL & CREW   -----------------------------------

public:
	/** The starting/new ship HULL durability value */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship")
		float MaxHullDurability;
	/** Current HULL durability value */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship")
		float CurrentHullDurability;


	/** Function for InitialHulStartingHullDurabilitylDurability */
	UFUNCTION(BlueprintPure, Category = "Ship")
		float GetMaxHullDurability();
	/** Function for InitialHullDurability */
	UFUNCTION(BlueprintPure, Category = "Ship")
		float GetCurrentHullDurability();
	/** Function to the update the Ship Hull Durability
	*   Parametr HullDurabilityChange  - is the amount to change HullDurabilty value and  - can be positive or negative. */
	UFUNCTION(BlueprintCallable, Category = "Ship")
		void UpdateHullDurability(float HullDurabilityChange);


};

