// Fill out your copyright notice in the Description page of Project Settings.

#include "Pirat.h"
#include "PiratPawn.h"
#include "PiratGameMode.h"
#include "CannonBallProjectile.h"
#include "TimerManager.h"

const FName APiratPawn::ShotRightBinding("ShotRight");

// Sets default values
APiratPawn::APiratPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	

	// Create mesh component for Ship
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShipStaticMesh(TEXT("StaticMesh'/Game/3DModels/Ship03_3D/Ship04.Ship04'"));
	ShipMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseShipMesh"));
	ShipMesh->SetStaticMesh(ShipStaticMesh.Object);
	RootComponent = ShipMesh;

	// Create Sphere component for Ship
//	VisibilityDistanceFromShip = CreateDefaultSubobject<USphereComponent>(TEXT("VisibilityDistanceSphere"));
//	VisibilityDistanceFromShip->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
//	VisibilityDistanceFromShip->SetSphereRadius(500.f);
	// VisibilityDistanceFromShip->IsOverlappingActor()

	// Cache our sound effect
	// Cannon shot sound
	static ConstructorHelpers::FObjectFinder<USoundBase>CannonShotAudio(TEXT("SoundWave'/Game/Audio/CannonLong_Audio.CannonLong_Audio'"));
	ShotSound = CannonShotAudio.Object;
	

	// Create a Camera SpringArm
	CameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	CameraSpringArm->SetupAttachment(RootComponent);
	CameraSpringArm->bAbsoluteRotation = true;	// Don't want rotate SpirngArm when Ship does
	CameraSpringArm->bDoCollisionTest = false;	// Don't want pull camera in - when collides with level
	CameraSpringArm->SetRelativeRotation(FRotator(-65.0f, 0.0f, 0.0f));
	CameraSpringArmMax = 5500.f;
	CameraSpringArmMin = 600.f;

	// USpringArmComponent::SocketName

	// Create Camera
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera0"));
	Camera->AttachToComponent(CameraSpringArm, FAttachmentTransformRules::KeepRelativeTransform);
	Camera->bUsePawnControlRotation = false;	//Camera doesn't rotate relative to SpringArm

	// Controll from main Player0
	AutoPossessPlayer = EAutoReceiveInput::Player0;
	//  Set Physic (gravity) for ship mesh
	//GetShipMesh()->SetSimulatePhysics(true);
	//GetShipMesh()->SetEnableGravity(true);
	//GetShipMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	


	//============  Default Ship Values	 ===========================================

		// Movement
//	bSailsPosition = false;

	// Affects Ship Speed - higher value increase speed
	ShipSpeedFactor = 1.0f; //(values 0.8 - 1.3, default = 1.0f)
	// Affects Ship Rotation - higher value increase rotation speed
	ShipRotationSpeed = 50.f;
	// Affects Ship Rotation - higher value increase ship reaction for input
	Maneuverability = 1.f;
	
		// Weapon
	// Cannons quantity on the ship
	CannonQuantity = 24;
	// Reload speed for cannon
	CannonReloadSpeed = 1.f;
	// Cannon shot possibility - false when reload
	bCanShot = true;
	// Current Connon type for ship
	CurrentCannon = ECannonType::ESlabe;

		//  Hull & Crew
	// starting base HullDurability for ship
	StartingHullDurability = 310.f;

}

// Called when the game starts or when spawned
void APiratPawn::BeginPlay()
{
	Super::BeginPlay();

	// Begin Play Sails Position is DOWN/false
//	bSailsPosition = false; // bSailsUp true - allow ship movement
}

// Called every frame (Ship Rotation , Movement, Inertia(plus/minus)
void APiratPawn::Tick( float DeltaSeconds )
{
	// Calculate change in rotation this frame
	FRotator DeltaRotation(0, 0, 0);
	DeltaRotation.Yaw = CurrentYawSpeed * DeltaSeconds;
	// Rotate ship
	AddActorLocalRotation(DeltaRotation);


	// Movement Speed depend sails position and PointofSail
	if ((bSailsPosition) && (!((PointOfSail > 150) && (PointOfSail <= 180))))
	{
		// Funtion call with delay
		GetWorldTimerManager().SetTimer(TimerHandle_DecreaseInertia, this, &APiratPawn::DecreaseInertia, 0.5f, true);
		// Move forwards (with sweep so we stop when we collide with things)
		const FVector LocalMove = FVector((GetCurrentShipSpeed()) * DeltaSeconds, 0.f, 0.f);
		AddActorLocalOffset(LocalMove, true);
		// Reset InertiaMinus Value
		InertiaMinus = ShipSpeedFactor;
	}
	else
	{
		// Funtion call with delay
		GetWorldTimerManager().SetTimer(TimerHandle_IncreaseInertia, this, &APiratPawn::IncreaseInertia, 0.2f, true);
		const FVector LocalMove = FVector((GetInertiaShipSpeed()) * DeltaSeconds, 0.f, 0.f);
		// Move forwards (with sweep so we stop when we collide with things)
		AddActorLocalOffset(LocalMove, true);
		// Reset InertiaPlus Value
		InertiaPlus = 0.1f;
	}
	

	
	// Camera ZOOM
	{
		if (bZoomingIn)
		{
			ZoomFactor += DeltaSeconds;
		}
		else
		{
			ZoomFactor -= DeltaSeconds;
		}
		ZoomFactor = FMath::Clamp(ZoomFactor, 0.0f, 1.0f);
		//Blend camera FOV and SpringArm lenght based on ZoomFactor
		//Camera->FieldOfView = FMath::Lerp(90.0f, 60.0f, ZoomFactor);
		CameraSpringArm->TargetArmLength = FMath::Lerp(CameraSpringArmMax, CameraSpringArmMin, ZoomFactor);
	}


	// Cannon weapon Fire
	const float ShotRightValue = GetInputAxisValue(ShotRightBinding);
	const FVector ShotDirection = FVector(0.f, ShotRightValue, 0.f);

	// call CannonShot funcion
	CannonShot(ShotDirection);

	Super::Tick(DeltaSeconds);
}

/*
void APiratPawn::NotifyHit(UPrimitiveComponent* MyComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalForce, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, OtherActor, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalForce, Hit);

	// Deflect along the surface when we collide.
	//FRotator CurrentRotation = GetActorRotation(RootComponent);
	//SetActorRotation(FQuat::Slerp(CurrentRotation.Quaternion(), HitNormal.ToOrientationQuat(), 0.025f));
}
*/

// Called to bind functionality to input
void APiratPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	// Zooming Event
	InputComponent->BindAction("ZoomIn", IE_Pressed, this, &APiratPawn::ZoomIn);
	InputComponent->BindAction("ZoomOut", IE_Released, this, &APiratPawn::ZoomOut);
	InputComponent->BindAction("SailsUp", IE_Pressed, this, &APiratPawn::Sails);
	InputComponent->BindAction("CannonShotRandom", IE_Pressed, this, &APiratPawn::CannonShotRandom);
	//InputComponent->BindAction("ShotRight", IE_Pressed, this, &APiratPawn::CannonShot);

	InputComponent->BindAxis("ShipYawRotation", this, &APiratPawn::CameraPitch);
	InputComponent->BindAxis(ShotRightBinding);
}

// Camera Zoom
void APiratPawn::ZoomIn()
{
	bZoomingIn = true;
}

// Camera Zoom
void APiratPawn::ZoomOut()
{
	bZoomingIn = false;
}

// Ship Movement and Rotation based on Input
void APiratPawn::CameraPitch(float AxisValue)
{
	// Target yaw speed is based on input - if Sails UP ShipRottaionSpeed / Sails DOWN ShipRotationSpeed / 5;
	float TargetYawSpeed = !bSailsPosition ? (AxisValue * ShipRotationSpeed / 4.f) : !((PointOfSail > 120) && (PointOfSail <= 180)) ? (AxisValue * ShipRotationSpeed) : (AxisValue * ShipRotationSpeed / 2.f);
	// Yew Speed Factor based on Sails position and current or inertia speed
	float YawSpeedFactor = Maneuverability + (CurrentShipSpeed / 2000.f);// : InertiaShipSpeed;
	// Smoothly interpolate to target rotation speed - 'Inertia - Drift'
	CurrentYawSpeed = FMath::FInterpTo(CurrentYawSpeed, TargetYawSpeed, GetWorld()->GetDeltaSeconds(), YawSpeedFactor);
}
//-- Sails Up or Down relative to movement
void APiratPawn::SetSailsPosition(bool SailsPosition)
{
	bSailsPosition = SailsPosition;
}
//-- Ship Speed Factor - higher value increase speed (values 0.5 - 1.5, default = 1.0f)
float APiratPawn::GetShipSpeedFactor()
{
	return ShipSpeedFactor;
}
//-- Function needed for INPUT 'SailsUp' 
void APiratPawn::Sails()
{
	if (!bSailsPosition)
	{
		bSailsPosition = true;
	}
	else
	{
		bSailsPosition = false;
	}
}
//-- ShipDirection - needed for PointOfSail()
FRotator APiratPawn::GetShipDirection()
{

	FRotator NewBaseShipRotation = GetActorRotation();
	return NewBaseShipRotation;
}
//-- Current Speed calculation pattern/template
float APiratPawn::GetCurrentShipSpeed()
{
	// pointer for APiratGameMode
	APiratGameMode* PiratMode = (APiratGameMode*)GetWorld()->GetAuthGameMode();
	// Ship speed template
	CurrentShipSpeed = PiratMode->GetWindSpeed() * GetPointOfSail() * ((InertiaPlus < 0.8) ? InertiaPlus : ShipSpeedFactor) * 80 ;
	
	return CurrentShipSpeed;
}
//-- Inertia Ship Speed calculation pattern/template
float APiratPawn::GetInertiaShipSpeed()
{
	// pointer for APiratGameMode
	APiratGameMode* PiratMode = (APiratGameMode*)GetWorld()->GetAuthGameMode();
	// Ship speed template
	InertiaShipSpeed = PiratMode->GetWindSpeed() * GetPointOfSail() * InertiaMinus * 80;

	return InertiaShipSpeed;
}
//-- Decrease Inertia Ship Speed
void APiratPawn::DecreaseInertia()
{
	InertiaMinus -= 0.1f;
	if (InertiaMinus < 0.05)
	{
		GetWorldTimerManager().PauseTimer(TimerHandle_DecreaseInertia);
	}
}
//-- Increase Inertia Ship Speed
void APiratPawn::IncreaseInertia()
{
	InertiaPlus += 0.1f;
	if (InertiaPlus > 0.8)
	{
		GetWorldTimerManager().PauseTimer(TimerHandle_IncreaseInertia);
	}
}
//-- Ralative speed to ship rotation against wind
float APiratPawn::GetPointOfSail()
{
	// pointer for APiratGameMode
	APiratGameMode* PiratMode = (APiratGameMode*)GetWorld()->GetAuthGameMode();
	// Ship rotation against wind in Yaw
	PointOfSail = PiratMode->GetWindDirection().Yaw - GetShipDirection().Yaw;
	
	// Clamp PointOfSail from -180 to 180
	if (PointOfSail < -180)
	{
		PointOfSail = 360 + PointOfSail;
	}
	if (PointOfSail > 180) 
	{
		PointOfSail = PointOfSail - 360;
	}
	
	// Change PointofSails to positive
	if (PointOfSail < 0)
	{
		PointOfSail = PointOfSail * -1;
	}
	
	// Ship speed related to the Point of sail 
	if (PointOfSail <= 45)
	{
		PointOfSailSpeed = 1.3f + (PointOfSail * 0.01f);
	}
	if ((PointOfSail > 45) && (PointOfSail <= 90))
	{
		PointOfSailSpeed = 1.75f - ((PointOfSail - 45) * 0.01f);
	}
	if ((PointOfSail > 90) && (PointOfSail <= 135))
	{
		PointOfSailSpeed = 1.3f - ((PointOfSail - 90) * 0.01f);
	}
	if ((PointOfSail > 135) && (PointOfSail <= 150))
	{
		PointOfSailSpeed = 0.85f - ((PointOfSail - 135) * 0.01f);
	}
	//Ship Speed factor - relative to PointOfSail
	return PointOfSailSpeed;
}
//-- Function Get - created  for HUD 'Get_Sails_Position_Text_0'
bool APiratPawn::GetSailsPosition()
{
		return bSailsPosition;
}
//---------- CannonShot -------------------
void APiratPawn::CannonShot(FVector ShotDirection)
{
	//
	if (bCanShot == true)
	{
		// do zamiany - when pressing fire stick in direction
		if (ShotDirection.SizeSquared() > 0.0f)
		{
			float CannonBallRandomLocationY = 0.f;
			for (int i = 0; i < (CannonQuantity / 2); i++)
			{
				// miejsce pojawienia sie kuli od burty
				float CannonBallRandomLocationX = FMath::RandRange(200.f, 300.f);
				// miejsce pojawienia sie kuli wzdluz burty- rozstawiane od srodka
				CannonBallRandomLocationY = CannonBallRandomLocationY + (i % 2 == 0 ? i * 26 : i * -26); // 26 space between cannons
				CannonBallStartLocation = FVector(CannonBallRandomLocationX , CannonBallRandomLocationY, 0.f);
				// kat strzalu wzgledem statku
				const FRotator YawRotation = ShotDirection.Y < 0 ?  FRotator(0.f, -90.f, 0.f) : FRotator(0.f, 90.f, 0.f);
				const FRotator ShotRotation = GetActorRotation() + YawRotation;
				//Spawn projectile at an offset from this pawn
				const FVector SpawnLocation = GetActorLocation() + ShotRotation.RotateVector(CannonBallStartLocation);

				UWorld* const World = GetWorld();
				if (World != NULL)
				{
					// Spawn cannon ball
					World->SpawnActor<ACannonBallProjectile>(SpawnLocation, ShotRotation);
				}

				World->GetTimerManager().SetTimer(TimerHandle_ShotPossibility, this, &APiratPawn::ShotPossibility, CannonReloadSpeed);
			}
			if (ShotSound != nullptr)
			{
				UGameplayStatics::PlaySoundAtLocation(this, ShotSound, GetActorLocation());
			}

			bCanShot = false;
		}
	}

}

// Cannon shot to the target test
void APiratPawn::CannonShotRandom()
{
	if (bCanShot == true)
	{
			float CannonBallRandomLocationY = 0.f;
			for (int i = 0; i < (CannonQuantity / 2); i++)
			{
				
				switch (CurrentCannon)
				{
					case ECannonType::ESlabe:
					{
						ShotPrecisionY = FMath::RandRange(85.f, 95.f);
						ShotPrecisionX = FMath::RandRange(-3.f, 3.f);
					}break;
					case ECannonType::EZwykle:
					{
						ShotPrecisionY = FMath::RandRange(86.f, 94.f);
						ShotPrecisionX = FMath::RandRange(-2.5f, 2.5f);
					}break;
					case ECannonType::EDobre:
					{
						ShotPrecisionY = FMath::RandRange(87.f, 93.f);
						ShotPrecisionX = FMath::RandRange(-2.f, 2.f);
					}break;
					case ECannonType::EMistrzowskie:
					{
						ShotPrecisionY = FMath::RandRange(88.f, 92.f);
						ShotPrecisionX = FMath::RandRange(-1.5f, 1.5f);
					}break;
					case ECannonType::ELegendarne:
					{
						ShotPrecisionY = FMath::RandRange(89.f, 91.f);
						ShotPrecisionX = FMath::RandRange(-1.f, 1.f);
					}break;
				}

				// miejsce pojawienia sie kuli od burty
				float CannonBallRandomLocationX = FMath::RandRange(200.f, 300.f);
				// miejsce pojawienia sie kuli wzdluz burty- rozstawiane od srodka
				CannonBallRandomLocationY = CannonBallRandomLocationY + (i % 2 == 0 ? i * 26 : i * -26); // 26 space between cannons
				CannonBallStartLocation = FVector(CannonBallRandomLocationX, CannonBallRandomLocationY, 0.f);
				// kat strzalu wzgledem statku
				const FRotator YawRotation = FRotator(ShotPrecisionX, ShotPrecisionY, 0.f);     // ShotDirection.Y < 0 ? FRotator(0.f, -90.f, 0.f) : FRotator(0.f, 90.f, 0.f);
				const FRotator ShotPrecision = GetActorRotation() + YawRotation;
				//Spawn projectile at an offset from this pawn
				const FVector SpawnLocation = GetActorLocation() + ShotPrecision.RotateVector(CannonBallStartLocation);

				UWorld* const World = GetWorld();
				if (World != NULL)
				{
					// Spawn cannon ball
				World->SpawnActor<ACannonBallProjectile>(SpawnLocation, ShotPrecision);
				}

				World->GetTimerManager().SetTimer(TimerHandle_ShotPossibility, this, &APiratPawn::ShotPossibility, CannonReloadSpeed);
			}
			if (ShotSound != nullptr)
			{
				UGameplayStatics::PlaySoundAtLocation(this, ShotSound, GetActorLocation());
			}

			bCanShot = false;
		}

}

// Is Shot possibly or not
void APiratPawn::ShotPossibility()
{
	bCanShot = true;
}


//-------------------   HULL & CREW   -----------------------------------

// Returns Starting hull durability
float APiratPawn::GetStartingHullDurability()
{
	return StartingHullDurability;
}
