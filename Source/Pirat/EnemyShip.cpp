// Fill out your copyright notice in the Description page of Project Settings.

#include "Pirat.h"
#include "EnemyShip.h"
#include "PiratGameMode.h"
#include "CannonBallProjectile.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"

const FName AEnemyShip::ShotRightBinding("ShotRight");

// Sets default values
AEnemyShip::AEnemyShip()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create mesh component for Ship
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ShipStaticMesh(TEXT("StaticMesh'/Game/3DModels/Ship01_3D/Ship01.Ship01'"));
	ShipMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseShipMesh"));
	ShipMesh->SetStaticMesh(ShipStaticMesh.Object);
	RootComponent = ShipMesh;

	// Create Sphere component for Ship
	//	VisibilityDistanceFromShip = CreateDefaultSubobject<USphereComponent>(TEXT("VisibilityDistanceSphere"));
	//	VisibilityDistanceFromShip->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
	//	VisibilityDistanceFromShip->SetSphereRadius(500.f);
	// VisibilityDistanceFromShip->IsOverlappingActor()

	// Cache our sound effect
	// Cannon shot sound
	static ConstructorHelpers::FObjectFinder<USoundBase>CannonShotAudio(TEXT("SoundWave'/Game/Audio/CannonLong_Audio.CannonLong_Audio'"));
	ShotSound = CannonShotAudio.Object;


	// Set Physic (gravity) for ship mesh
	//GetShipMesh()->SetSimulatePhysics(true);
	//GetShipMesh()->SetEnableGravity(true);
	//GetShipMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);



	//============  Default Ship Values	 ===========================================

	// Movement
	//	bSailsPosition = false;

	// Affects Ship Speed - higher value increase speed	
	ShipSpeedFactor = 1.0f; //(values 0.8 - 1.3, default = 1.0f)
	// Affects Ship Rotation - higher value increase rotation speed
	ShipRotationSpeed = 50.f;
	// Affects Ship Rotation - higher value increase ship reaction for input
	Maneuverability = 1.f;

	// Weapon
	// Cannons quantity on the ship
	CannonQuantity = 24;
	// Reload speed for cannon
	CannonReloadSpeed = 1.f;
	// Cannon shot possibility - false when reload
	bCanShot = true;


	//  Hull & Crew
	// starting base HullDurability for Enemy ship
	StartingEnemyHullDurability = 1000.f;
}

// Called when the game starts or when spawned
void AEnemyShip::BeginPlay()
{
	Super::BeginPlay();

	// starting base HullDurability for Enemy ship
	StartingEnemyHullDurability = 1000.f;

	// Begin Play Sails Position is UP/true - allow ship movement
	bSailsPosition = true; 
}

// Called every frame (Ship Rotation , Movement, Inertia(plus/minus)
void AEnemyShip::Tick(float DeltaSeconds)
{
	// Calculate change in rotation this frame
	FRotator DeltaRotation(0, 0, 0);
	DeltaRotation.Yaw = CurrentYawSpeed * DeltaSeconds;
	// Rotate ship
	AddActorLocalRotation(DeltaRotation);


	// Movement Speed depend sails position and PointofSail
	if ((bSailsPosition) && (!((PointOfSail > 150) && (PointOfSail <= 180))))
	{
		// Funtion call with delay
		const FVector LocalMove = FVector((GetCurrentShipSpeed()) * DeltaSeconds, 0.f, 0.f);
		AddActorLocalOffset(LocalMove, true);

	}





	// Cannon weapon Fire
	const float ShotRightValue = GetInputAxisValue(ShotRightBinding);
	const FVector ShotDirection = FVector(0.f, ShotRightValue, 0.f);

	// call CannonShot funcion
	CannonShot(ShotDirection);

	Super::Tick(DeltaSeconds);
}


void AEnemyShip::NotifyHit(class UPrimitiveComponent* MyComp, class AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	// Deflect along the surface when we collide.
	//FRotator CurrentRotation = GetActorRotation(RootComponent);
	//SetActorRotation(FQuat::Slerp(CurrentRotation.Quaternion(), HitNormal.ToOrientationQuat(), 0.025f));
}

// Ship Movement and Rotation based on Input
void AEnemyShip::CameraPitch(float AxisValue)
{
	// Target yaw speed is based on input - if Sails UP ShipRottaionSpeed / Sails DOWN ShipRotationSpeed / 5;
	float TargetYawSpeed = !bSailsPosition ? (AxisValue * ShipRotationSpeed / 4.f) : !((PointOfSail > 120) && (PointOfSail <= 180)) ? (AxisValue * ShipRotationSpeed) : (AxisValue * ShipRotationSpeed / 2.f);
	// Yew Speed Factor based on Sails position and current or inertia speed
	float YawSpeedFactor = Maneuverability + (CurrentShipSpeed / 2000.f);// : InertiaShipSpeed;
																		 // Smoothly interpolate to target rotation speed - 'Inertia - Drift'
	CurrentYawSpeed = FMath::FInterpTo(CurrentYawSpeed, TargetYawSpeed, GetWorld()->GetDeltaSeconds(), YawSpeedFactor);
}

//-- Ship Speed Factor - higher value increase speed (values 0.5 - 1.5, default = 1.0f)
float AEnemyShip::GetShipSpeedFactor()
{
	return ShipSpeedFactor;
}

//-- ShipDirection - needed for PointOfSail()
FRotator AEnemyShip::GetShipDirection()
{

	FRotator NewBaseShipRotation = GetActorRotation();
	return NewBaseShipRotation;
}
//-- Current Speed calculation pattern/template
float AEnemyShip::GetCurrentShipSpeed()
{
	// pointer for APiratGameMode
	APiratGameMode* PiratMode = (APiratGameMode*)GetWorld()->GetAuthGameMode();
	// Ship speed template
	CurrentShipSpeed = PiratMode->GetWindSpeed() * GetPointOfSail() * 25; // ((InertiaPlus < 0.8) ? InertiaPlus : ShipSpeedFactor) * 80;

	return CurrentShipSpeed;
}


//-- Ralative speed to ship rotation against wind
float AEnemyShip::GetPointOfSail()
{
	// pointer for APiratGameMode
	APiratGameMode* PiratMode = (APiratGameMode*)GetWorld()->GetAuthGameMode();
	// Ship rotation against wind in Yaw
	PointOfSail = PiratMode->GetWindDirection().Yaw - GetShipDirection().Yaw;

	// Clamp PointOfSail from -180 to 180
	if (PointOfSail < -180)
	{
		PointOfSail = 360 + PointOfSail;
	}
	if (PointOfSail > 180)
	{
		PointOfSail = PointOfSail - 360;
	}

	// Change PointofSails to positive
	if (PointOfSail < 0)
	{
		PointOfSail = PointOfSail * -1;
	}

	// Ship speed related to the Point of sail 
	if (PointOfSail <= 45)
	{
		PointOfSailSpeed = 1.3f + (PointOfSail * 0.01f);
	}
	if ((PointOfSail > 45) && (PointOfSail <= 90))
	{
		PointOfSailSpeed = 1.75f - ((PointOfSail - 45) * 0.01f);
	}
	if ((PointOfSail > 90) && (PointOfSail <= 135))
	{
		PointOfSailSpeed = 1.3f - ((PointOfSail - 90) * 0.01f);
	}
	if ((PointOfSail > 135) && (PointOfSail <= 150))
	{
		PointOfSailSpeed = 0.85f - ((PointOfSail - 135) * 0.01f);
	}
	//Ship Speed factor - relative to PointOfSail
	return PointOfSailSpeed;
}

//---------- CannonShot -------------------
void AEnemyShip::CannonShot(FVector ShotDirection)
{
	//
	if (bCanShot == true)
	{
		// do zamiany - when pressing fire stick in direction
		if (ShotDirection.SizeSquared() > 0.0f)
		{
			float CannonBallRandomLocationY = 0.f;
			for (int i = 0; i < (CannonQuantity / 2); i++)
			{
				// miejsce pojawienia sie kuli od burty
				float CannonBallRandomLocationX = FMath::RandRange(200.f, 300.f);
				// miejsce pojawienia sie kuli wzdluz burty- rozstawiane od srodka
				CannonBallRandomLocationY = CannonBallRandomLocationY + (i % 2 == 0 ? i * 26 : i * -26); // 26 space between cannons
				CannonBallStartLocation = FVector(CannonBallRandomLocationX, CannonBallRandomLocationY, 0.f);
				// kat strzalu wzgledem statku
				const FRotator YawRotation = ShotDirection.Y < 0 ? FRotator(0.f, -90.f, 0.f) : FRotator(0.f, 90.f, 0.f);
				const FRotator ShotRotation = GetActorRotation() + YawRotation;
				//Spawn projectile at an offset from this pawn
				const FVector SpawnLocation = GetActorLocation() + ShotRotation.RotateVector(CannonBallStartLocation);

				UWorld* const World = GetWorld();
				if (World != NULL)
				{
					// Spawn cannon ball
					World->SpawnActor<ACannonBallProjectile>(SpawnLocation, ShotRotation);
				}

				World->GetTimerManager().SetTimer(TimerHandle_ShotPossibility, this, &AEnemyShip::ShotPossibility, CannonReloadSpeed);
			}
			if (ShotSound != nullptr)
			{
				UGameplayStatics::PlaySoundAtLocation(this, ShotSound, GetActorLocation());
			}

			bCanShot = false;
		}
	}

}
// Is Shot possibly or not
void AEnemyShip::ShotPossibility()
{
	bCanShot = true;
}


//-------------------   HULL & CREW   -----------------------------------

// Returns Starting hull durability
float AEnemyShip::GetStartingEnemyHullDurability()
{
	return StartingEnemyHullDurability;
}

//Return Current hull durability
float AEnemyShip::GetCurrentEnemyHullDurability()
{
	return CurrentEnemyHullDurability;
}

// Called when Hull Durability is incresed or decresed
void AEnemyShip::UpdateEnemyHullDurability(float EnemyHullDurabilityChange)
{
	CurrentEnemyHullDurability = CurrentEnemyHullDurability + EnemyHullDurabilityChange;
}
