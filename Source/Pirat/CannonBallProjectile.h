// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CannonBallProjectile.generated.h"

UCLASS()
class PIRAT_API ACannonBallProjectile : public AActor
{
	GENERATED_BODY()
	


public:	
	// Sets default values for this actor's properties
	ACannonBallProjectile();
	/** Returns CannonBallCollisionSphere subobject **/
	FORCEINLINE class UStaticMeshComponent* GetCannonBallStaticMesh() const { return CannonBallMesh; }
	/** Returns CannonBallProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetCannonBallProjectileMovement() const { return CannonBallProjectileMovement; }

	/** Function called when projectile hits something */
//	void OnHit(UPrimitiveComponent* ThisComp, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);  // orginalne z Myproject3
	void NotifyHit(UPrimitiveComponent* MyComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalForce, const FHitResult& Hit);
//	virtual void NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalForce, const FHitResult& Hit) override; // orginalne z 'virtual i override' z strategy game
private:
	/** Alternative collision component as  a Sphere representation */
	//UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	//class USphereComponent* CannonBallCollisionSphere;

	/** Static Mesh component as Sphere collision representation */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Projectile, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* CannonBallMesh;
	/** CannonBall Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, Meta = (AllowPrivateAccess = "true"))
	class UProjectileMovementComponent* CannonBallProjectileMovement;

};
