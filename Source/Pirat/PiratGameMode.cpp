// Fill out your copyright notice in the Description page of Project Settings.

#include "Pirat.h"
#include "PiratGameMode.h"
#include "PiratPawn.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

APiratGameMode::APiratGameMode()
{
	//set Default Pawn Class to my character class
	//DefaultPawnClass = APiratPawn::StaticClass();
	//HUDClass = APiratPawn::StaticClass();


	// base Hull Decay Rate
	HullDecayRate = 0.01f;

}

void APiratGameMode::BeginPlay()
{
	Super::BeginPlay();

	// WindDirection when begin play
	int RandomDirection = FMath::FRandRange(-155.0f, -25.0f);
	WindDirection.Yaw = RandomDirection;

	// WindSpeed when begin play
	WindSpeed = FMath::FRandRange(3.0f, 6.0f);

	// First wind (speed/direction) change after begin play.
	WindSpeedChangeDelay = FMath::FRandRange(WindSpeedChangeDelayMin, WindSpeedChangeDelayMax);
	GetWorldTimerManager().SetTimer(WindChange_TimerHandle, this, &APiratGameMode::WindChange, WindSpeedChangeDelay);

	// --------------  Hull & Crew ----------------------------------------

	APiratPawn* PiratPawn = Cast<APiratPawn>(UGameplayStatics::GetPlayerPawn(this, 0));
	//Check - we are using PiratPawn
	if (PiratPawn)
	{
		//  Hull & Crew
		// starting base HullDurability for ship + modifiers
		MaxHullDurability = PiratPawn->GetStartingHullDurability();
	}


	// current HullDurability for ship  is a starting HullDurability
		CurrentHullDurability = MaxHullDurability;

}

void APiratGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	APiratPawn* PiratPawn = Cast<APiratPawn>(UGameplayStatics::GetPlayerPawn(this, 0));
	//Check - we are using PiratPawn
	if (PiratPawn)
	{
		// if HullDurability is positive
		if (GetCurrentHullDurability() > 0)
		{
			// decrese the HullDurability - by using HeullDecayRate
			UpdateHullDurability(-DeltaTime*HullDecayRate*(GetMaxHullDurability()));
		}
	}

}

FRotator APiratGameMode::GetWindDirection()
{
	return WindDirection;
}

uint8 APiratGameMode::GetWindSpeed()
{
	return WindSpeed;
}

// Template for Wind Speed and Direction
void APiratGameMode::WindChange()
{
	UWorld* World = GetWorld();
	if (World)
	{
				// WIND DIRECTION in World

		WindDirectionChange.Yaw = FMath::FRandRange(-5.0f, 5.0f);
		if (WindDirection.Yaw < -181)
		{
			WindDirection.Yaw = 179;
		}
		if(WindDirection.Yaw > 181)
		{
			WindDirection.Yaw = -179;
		}
		else
		{
			WindDirection = WindDirection + WindDirectionChange;
		}
		
		WindDirectionChangeDelay = FMath::FRandRange(WindDirectionChangeDelayMin, WindDirectionChangeDelayMax);
		GetWorldTimerManager().SetTimer(WindChange_TimerHandle, this, &APiratGameMode::WindChange, WindDirectionChangeDelay);

		

//------------------   WIND SPEED in World

		int RandomRoll1_10 = FMath::RandRange(1, 10);
		switch (WindSpeed)
		{
		case  1:
		{
			if (RandomRoll1_10 > 1)
			{
				WindSpeed += 1;
			}
		}break;
		case 2:
		{
			if (RandomRoll1_10 < 2)
			{
				WindSpeed -= 1;
			}
			if (RandomRoll1_10 > 3)
			{
				WindSpeed += 1;
			}
		}break;
		case 3:
		{
			if (RandomRoll1_10 < 3)
			{
				WindSpeed -= 1;
			}
			if (RandomRoll1_10 > 5)
			{
				WindSpeed += 1;
			}
		}break;
		case 4:
		{
			if (RandomRoll1_10 < 4)
			{
				WindSpeed -= 1;
			}
			if (RandomRoll1_10 > 7)
			{
				WindSpeed += 1;
			}
		}break;
		case 5:
		{
			if (RandomRoll1_10 < 4)
			{
				WindSpeed -= 1;
			}
			if (RandomRoll1_10 > 7)
			{
				WindSpeed += 1;
			}
		}break;
		case 6:
		{
			if (RandomRoll1_10 > 8)
			{
				WindSpeed += 1;
			}
			if (RandomRoll1_10 < 6)
			{
				WindSpeed -= 1;
			}
		}break;
		case 7:
		{
			if (RandomRoll1_10 > 9)
			{
				WindSpeed += 1;
			}
			if (RandomRoll1_10 < 8)
			{
				WindSpeed -= 1;
			}
		}break;
		case 8:
		{
			if (RandomRoll1_10 < 10)
			{
				WindSpeed -= 1;
			}
		}break;
		default:
		{
			WindSpeed = 8;
		}
		break;
		}
		WindSpeedChangeDelay = FMath::FRandRange(WindSpeedChangeDelayMin, WindSpeedChangeDelayMax);
		GetWorldTimerManager().SetTimer(WindChange_TimerHandle, this, &APiratGameMode::WindChange, WindSpeedChangeDelay);
		
	}
}

//-------------------   HULL & CREW   -----------------------------------

// Returns Starting hull durability
float APiratGameMode::GetMaxHullDurability()
{
	return MaxHullDurability;
}

//Return Current hull durability
float APiratGameMode::GetCurrentHullDurability()
{
	return CurrentHullDurability;
}

// Called when Hull Durability is incresed or decresed
void APiratGameMode::UpdateHullDurability(float HullDurabilityChange)
{
	CurrentHullDurability = CurrentHullDurability + HullDurabilityChange;
}