// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "EnemyShip.generated.h"

UCLASS()
class PIRAT_API AEnemyShip : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemyShip();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//=====================================================================================================

	//    Camera - Movement - Rotation

public:
	/**  */
	virtual void NotifyHit(class UPrimitiveComponent* MyComp, class AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;
	/** Return ShipMesh subobject */
	FORCEINLINE class UStaticMeshComponent* GetShipMesh() const { return ShipMesh; }
	/** Return VisibilityDistanceFromShip subobject */
	//		FORCEINLINE class USphereComponent* GetVisibilityDistanceFromShip() const { return VisibilityDistanceFromShip; }




	//  CAMERA --------------------------------------

	/** Camera Input - Functions */
	void CameraPitch(float AxisValue);



	//   MOVEMENT -----------------------------------------------------

public:
	// Movement public Functions
	/** Current Speed calculation pattern/template */
	UFUNCTION(BlueprintPure, Category = "Ship")
		float GetCurrentShipSpeed();


protected:
	/** Ship speed component - Function */
	/** Ship Speed Factor - higher value increase speed (values 0.5 - 1.5, default = 1.0f) */
	UFUNCTION(BlueprintPure, Category = "Ship")
		float GetShipSpeedFactor();


protected:
	//  MainShip Movement - Function 
	/** Ship Yaw Rotation */
	FRotator GetShipDirection();



	/** Ralative speed to ship rotation against wind */
	UFUNCTION(BlueprintPure, Category = "Ship")
		float GetPointOfSail();


private:
	/** The Ship mesh component */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", Meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* ShipMesh;
	/** Distance to spot another ships */
	//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", Meta = (AllowPrivateAccess = "true"))
	class USphereComponent* VisibilityDistanceFromShip;

	/** Ship Speed Component - Affects Ship Speed - higher value increase speed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", Meta = (AllowPrivateAccess = "true"))
		float ShipSpeedFactor;
	/** Ship Speed Component - Affects Ship Rotation - higher value increase rotation speed. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", Meta = (AllowPrivateAccess = "true"))
		float ShipRotationSpeed;
	/** Ship Speed Component - Affects Ship Rotation - higher value increase ship reaction for input */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", Meta = (AllowPrivateAccess = "true"))
		float Maneuverability;

private:

	//  MainShip Movement - Variables
	//	/** Sails position - Allow or not Movement - default true (sails UP)*/
	bool bSailsPosition;
	/** Current yaw Rotation speed */
	float CurrentYawSpeed;
	/** Current Ship speed */
	float CurrentShipSpeed;
	/** Yaw - Ship rotation against wind */
	float PointOfSail;
	/** Ship Speed factor - relative to PointOfSail */
	float PointOfSailSpeed;



	// NEW Projectile  ----------------------------------------------------

public:
	/** Offset from the ships location to spawn projectiles */
	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
		FVector CannonBallStartLocation; // gunoffset = CannonDirection
										 /** Reload speed for cannon */
	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
		float CannonReloadSpeed;
	/** Sound to play each time we fire */
	UPROPERTY(Category = Audio, EditAnywhere, BlueprintReadWrite)
	class USoundBase* ShotSound;

	/* Fire a shot in the specified direction */
	void CannonShot(FVector ShotDirection);
	/* Handler for the fire timer expiry */
	void ShotPossibility();
	// Static names for axis bindings
	static const FName ShotRightBinding;
	/** Cannons quantity on the ship */
	short CannonQuantity;


private:
	/* Flag to control firing  */
	uint32 bCanShot : 1;

	/** Handle for efficient management of ShotTimerExpired timer */
	FTimerHandle TimerHandle_ShotPossibility;



	//-------------------   HULL & CREW   -----------------------------------

public:
	/** The starting/new EnemyShip HULL durability value */
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyShip")
			float StartingEnemyHullDurability;
	/** Current HULL durability value */
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyShip")
			float CurrentEnemyHullDurability;


	/** Function for InitialHullDurability */
		UFUNCTION(BlueprintPure, Category = "EnemyShip")
			float GetStartingEnemyHullDurability();
	/** Function for InitialHullDurability */
		UFUNCTION(BlueprintPure, Category = "EnemyShip")
			float GetCurrentEnemyHullDurability();
	/** Function to the update the EnemyShip Hull Durability
	*   Parametr HullDurabilityChange  - is the amount to change HullDurabilty value and  - can be positive or negative. */
		UFUNCTION(BlueprintCallable, Category = "EnemyShip")
		void UpdateEnemyHullDurability(float EnemyHullDurabilityChange);


};