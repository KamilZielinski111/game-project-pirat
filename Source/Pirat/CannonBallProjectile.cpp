// Fill out your copyright notice in the Description page of Project Settings.

#include "Pirat.h"
#include "CannonBallProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"


// Sets default values
ACannonBallProjectile::ACannonBallProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	

	// Create static mesh component for projectile
	CannonBallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CannonBallMesh"));
	// Static reference to mesh to use for projectile
	static ConstructorHelpers::FObjectFinder<UStaticMesh>CannonBallMeshes(TEXT("StaticMesh'/Game/Meshes/TwinStickProjectile.TwinStickProjectile'"));
	if (CannonBallMeshes.Succeeded())
	{
		CannonBallMesh->SetStaticMesh(CannonBallMeshes.Object);
	}
	CannonBallMesh->SetupAttachment(RootComponent);
	CannonBallMesh->BodyInstance.SetCollisionProfileName("Projectile");
//	CannonBallMesh->OnComponentHit.AddDynamic(this, &ACannonBallProjectile::OnHit);
	RootComponent = CannonBallMesh;

	// CannonBallProjectileMovement is a component to control this projectile movement
	CannonBallProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("CannonBallProjectileMovement"));
	CannonBallProjectileMovement->UpdatedComponent = CannonBallMesh;
	CannonBallProjectileMovement->InitialSpeed = 1000.f;
	CannonBallProjectileMovement->MaxSpeed = 1000.f;
	CannonBallProjectileMovement->bRotationFollowsVelocity = true;
	CannonBallProjectileMovement->bShouldBounce = false;	// Bounce off
	CannonBallProjectileMovement->ProjectileGravityScale = 0.02f; // Gravity off

	// Destroy after 3 seconds by default
	InitialLifeSpan = 8.f;
}

void ACannonBallProjectile::NotifyHit(UPrimitiveComponent* MyComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalForce, const FHitResult& Hit)
{

	Super::NotifyHit(MyComp, OtherActor, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalForce, Hit);

	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(GetVelocity() * 20.0f, GetActorLocation());
	}

	Destroy();


	// Only destroy projectile if we hit a physics
//	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
//	{
//		OtherComp->AddImpulseAtLocation(GetVelocity() * 20.0f, GetActorLocation());

//		Destroy();
//	}
}