// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/Pawn.h"
#include "PiratPawn.generated.h"

/** Enum to store types of Cannons  */
UENUM(BlueprintType)
enum class ECannonType : uint8
{
	ESlabe,
	EZwykle,
	EDobre,
	EMistrzowskie,
	ELegendarne
};

UCLASS()
class PIRAT_API APiratPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APiratPawn();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
//=====================================================================================================
	
	//    Camera - Movement - Rotation

public:
	/**  */
//        virtual void NotifyHit(UPrimitiveComponent* MyComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalForce, const FHitResult& Hit);
	/** Return ShipMesh subobject */
		FORCEINLINE class UStaticMeshComponent* GetShipMesh() const { return ShipMesh; }
	/** Return VisibilityDistanceFromShip subobject */
//		FORCEINLINE class USphereComponent* GetVisibilityDistanceFromShip() const { return VisibilityDistanceFromShip; }
	/** Handle for managment of DecreaseInertia timer */
		FTimerHandle TimerHandle_DecreaseInertia;
	/** Handle for managment of IncreaseInertia timer */
		FTimerHandle TimerHandle_IncreaseInertia;
	

		//  CAMERA --------------------------------------

protected:
	/** Camera Input - Variables */
		bool bZoomingIn;
		float ZoomFactor;
	/**  Maximum distance for Camera Sprng Arm when zoomout */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera", meta = (BlueprintProtected = "true"))
		float CameraSpringArmMax;
	/** Minimum distance for Camera Sprng Arm when zoomin */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera", meta = (BlueprintProtected = "true"))
		float CameraSpringArmMin;
		//FVector2D CameraInput;

	
	/** Camera Input - Functions */
		void ZoomIn();
		void ZoomOut();
		void CameraPitch(float AxisValue);



private:
	/** Create Camera and Spring Arm for camera. */
	UPROPERTY(EditAnywhere)
	class USpringArmComponent* CameraSpringArm;
	class UCameraComponent* Camera;
	


	//   MOVEMENT -----------------------------------------------------

public:
		// Movement public Functions
	/** Current Speed calculation pattern/template */
	UFUNCTION(BlueprintPure, Category = "Ship")
		float GetCurrentShipSpeed();
	/** Function Get created  for HUD 'Get_Sails_Position_Text_0' */
	UFUNCTION(BlueprintPure, Category = "Ship")
		bool GetSailsPosition();
	/** Sails Up or Down relative to movement */
	UFUNCTION(BlueprintCallable, Category = "Ship")
		void SetSailsPosition(bool SailsPosition);




	/** Function needed for INPUT 'SailsUp' */
	UFUNCTION(BlueprintCallable, Category = "Ship") //moved from protected
		void Sails();


	
protected:
	/** Ship speed component - Function */
	/** Ship Speed Factor - higher value increase speed (values 0.5 - 1.5, default = 1.0f) */
	UFUNCTION(BlueprintPure, Category = "Ship")
		float GetShipSpeedFactor();
	/** Inertia Ship Speed calculation pattern/template */
	UFUNCTION(BlueprintPure, Category = "Ship")
		float GetInertiaShipSpeed();
	/** Decrease Inertia Ship Speed */
	UFUNCTION()
		void DecreaseInertia();
	/** Increase Inertia Ship Speed */
	UFUNCTION()
		void IncreaseInertia();

protected:
		//  MainShip Movement - Function 
	/** Ship Yaw Rotation */
		FRotator GetShipDirection();
	


	/** Ralative speed to ship rotation against wind */
	UFUNCTION(BlueprintPure, Category = "Ship")
		float GetPointOfSail();
//	/** Function needed for INPUT 'SailsUp' */
//	UFUNCTION(BlueprintCallable, Category = "Ship")
//		void Sails();
	

private:
	/** The Ship mesh component */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", Meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* ShipMesh;
	/** Distance to spot another ships */
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", Meta = (AllowPrivateAccess = "true"))
		class USphereComponent* VisibilityDistanceFromShip;

	/** Ship Speed Component - Affects Ship Speed - higher value increase speed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", Meta = (AllowPrivateAccess = "true"))
		float ShipSpeedFactor;
	/** Ship Speed Component - Affects Ship Rotation - higher value increase rotation speed. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", Meta = (AllowPrivateAccess = "true"))
		float ShipRotationSpeed;
	/** Ship Speed Component - Affects Ship Rotation - higher value increase ship reaction for input */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", Meta = (AllowPrivateAccess = "true"))
		float Maneuverability;

private:

		//  MainShip Movement - Variables
//	/** Sails position - Allow or not Movement - default false(sails down)*/
		bool bSailsPosition;
	/** Current yaw Rotation speed */
		float CurrentYawSpeed;
	/** Current Ship speed */
		float CurrentShipSpeed;
	/** Inertia Ship speed */
		float InertiaShipSpeed;
	/** Yaw - Ship rotation against wind */
		float PointOfSail;
	/** Ship Speed factor - relative to PointOfSail */
		float PointOfSailSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", Meta = (AllowPrivateAccess = "true"))
	/** Ship Speed factor for CurrentShipSpeed */
		float InertiaMinus;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", Meta = (AllowPrivateAccess = "true"))
	/** Ship Speed factor for InertiaShipSpeed */
		float InertiaPlus;



	// --------------   CANNON   --------------------------------------------------

public:
	/** Offset from the ships location to spawn projectiles */
	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
		FVector CannonBallStartLocation; // gunoffset = CannonDirection
	/** Reload speed for cannon */
	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
		float CannonReloadSpeed;
	/** Sound to play each time we fire */
	UPROPERTY(Category = Audio, EditAnywhere, BlueprintReadWrite)
	class USoundBase* ShotSound;
	
	/* Fire a shot in the specified direction */
	void CannonShot (FVector ShotDirection);
	/** Cannon ball shot in the specific direction depend on CannonType */
	void CannonShotRandom();

	/* Handler for the fire timer expiry */
	void ShotPossibility();
	// Static names for axis bindings
	static const FName ShotRightBinding;
	/** Cannons quantity on the ship */
	short CannonQuantity;


private:
	/* Flag to control firing  */
	uint32 bCanShot : 1;
	/** Cannon shot precision X ( dispertion 6 f )*/
	float ShotPrecisionY;
	/** Cannon shot precision Y ( dispertion 10 f )*/
	float ShotPrecisionX;
	/** Cannon cerrently used ( from CannonType Enum ) */
	ECannonType CurrentCannon;


	/** Handle for efficient management of ShotTimerExpired timer */
	FTimerHandle TimerHandle_ShotPossibility;


	//-------------------   HULL & CREW   -----------------------------------

public:
	/** The starting/new ship HULL durability value */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship")
		float StartingHullDurability;

	/** Function for InitialHullDurability */
	UFUNCTION(BlueprintPure, Category = "Ship")
		float GetStartingHullDurability();

};
